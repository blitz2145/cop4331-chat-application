package ChatAppGUI;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import ChatApp.Event;
import ChatApp.User;

/**
 * This class displays the history of messages with a contact.
 * This class uses the Decorator pattern, where the JScrollPane 
 * is the Decorator for the JList (Concrete Component)
 */
public class HistoryView extends CWindow {
	
	public HistoryView(final User user, final GUIController controller) {
		this.controller = controller;
		this.user  = user;
		reverse = false;
		repaint();
	}
	@Override
	public void repaint() {
		if(user.getMessageHistory().length == 0) {
			//JOptionPane.showMessageDialog(frame, "There is no message history for the selected contact");
		} else {
			frame = new JFrame();

			historyList = new JList<String>(user.getMessageHistory());
			//need action listeners
			sortHistoryButton = new JButton("Sort");
			
			sortHistoryButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if(reverse == true) {
						reverse = false;
					} else {
						reverse = true;
					}
					user.sortHistory(reverse);
				}
			});

			JScrollPane scrollPane = new JScrollPane(historyList);
			JPanel buttonPanel = new JPanel();
			buttonPanel.setLayout(new GridLayout(1,2));
			buttonPanel.add(sortHistoryButton);

			frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
			scrollPane.setPreferredSize(new Dimension(600,200));
			frame.add(scrollPane);
			frame.add(buttonPanel);
			frame.pack();
			frame.addWindowListener(new WindowAdapter() {
				public void windowClosing(WindowEvent e) {
					controller.update(null, Event.WINDOW_CLOSED);
				}
			});
			frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			frame.setVisible(true);
		}
	}
	
	private JList<String> historyList;
	private JButton sortHistoryButton;
	private User user;
	private GUIController controller;
	private boolean reverse;
}
