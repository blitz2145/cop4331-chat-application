package ChatAppGUI;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import ChatApp.Event;
import ChatApp.User;

/**
 * Shows the current dialog between the user and a contact.
 * This class uses the Decorator pattern, where the JScrollPane 
 * is the Decorator for the JList (Concrete Component)
 */
public class ChatWindow extends CWindow {
	/**
	 * Constructs a new ChatWindow
	 * @param user 
	 * @param controller
	 */
	public ChatWindow(final User user, final GUIController controller) {
		this.user = user;
		this.controller = controller;
		reverse = false;
		repaint();
	}
	
	@Override
	public void repaint() {
		frame = new JFrame();
		
		//create components
		messageField = new JTextField(50);
		messageList = new JList<String>(user.getCurrentMessages());
		sendMessageButton = new JButton("Send Message");
		historySortButton = new JButton("Sort Messages");
		
		//add action listeners
		sendMessageButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (messageField.getText().isEmpty()) {
					JOptionPane.showMessageDialog(frame, "No message entered!");
				} else {
					user.sendMessage(messageField.getText());
				}
			}
		});
		
		historySortButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (user.getCurrentMessages().length == 0) {
					JOptionPane.showMessageDialog(frame, "No messages available to sort!");
				} else {
					if(reverse == true) {
						reverse = false;
					} else {
						reverse = true;
					}
					user.sortMessages(reverse);
				}
			}
		});
		
		messagePane = new JScrollPane(messageList);
		messagePane.setPreferredSize(new Dimension(600,200));
		
		buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(1,2));
		
		buttonPanel.add(sendMessageButton);
		buttonPanel.add(historySortButton);
		
		frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
		frame.add(messagePane);
		frame.add(messageField);
		frame.add(buttonPanel);
		frame.pack();
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				user.endChatSession();
				controller.update(null, Event.WINDOW_CLOSED);
			}
		});
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setVisible(true);
	}

	private JPanel buttonPanel;
	private JScrollPane messagePane;
	private JTextField messageField;
	private JList<String> messageList;
	private JButton sendMessageButton;
	private JButton historySortButton;
	private User user;
	private GUIController controller;
	private boolean reverse;
}
