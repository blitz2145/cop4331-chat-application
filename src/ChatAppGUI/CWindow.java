package ChatAppGUI;
import javax.swing.*;

/**
 * This class represents a window that is used in the chat application
 */
public class CWindow {
	protected JFrame frame;
	
	/**
	 * Closes the CWindow object and frees resources
	 */
	public void dispose() {
		frame.dispose();
	}
	
	/**
	 * Repaints the CWindow object
	 */
	public void repaint() {
		frame.repaint();
	}
}
