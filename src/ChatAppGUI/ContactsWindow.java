package ChatAppGUI;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import ChatApp.Event;
import ChatApp.User;

/**
 * Shows the user's contacts in a list view.
 * This class uses the Decorator pattern, where the JScrollPane 
 * is the Decorator for the JList (Concrete Component)
 */
public class ContactsWindow extends CWindow {

	/**
	 * Constructs a new ContactsWindow
	 * @param user
	 * @param controller
	 */
	public ContactsWindow(final User user, final GUIController controller) {
		this.user = user;
		this.controller = controller;
		repaint();
	}

	@Override
	public void repaint() {
		frame = new JFrame();
		//create JList to display contacts' usernames
		if(user.getContactNames() == null) {
			list = new JList<String>();
		} else {
			list = new JList<String>(user.getContactNames());
		}
		//center usernames
		DefaultListCellRenderer renderer = (DefaultListCellRenderer)list.getCellRenderer();  
		renderer.setHorizontalAlignment(JLabel.CENTER);

		//get selection model and add selection listener
		ListSelectionModel listSelectionModel = list.getSelectionModel();
		listSelectionModel.addListSelectionListener(new ListSelectionListener() {
			//implement interface method
			public void valueChanged(ListSelectionEvent e) {
				//don't get value while it is changing
				if(!e.getValueIsAdjusting()) {
					//get selection model from event and set to single selection mode
					ListSelectionModel lsm = (ListSelectionModel)e.getSource();
					lsm.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
					//get index of selection and pass to user in order to select contact
					int index = lsm.getMinSelectionIndex();
					user.selectContact(index);
					//System.out.println("Selected index = " + index);
				}
			}
		});

		//create buttons
		viewHistoryButton = new JButton("View Message History");
		addContactButton = new JButton("Add Contact");
		removeContactButton = new JButton("Remove Contact");
		sendMessageButton = new JButton("Send Message");

		//add action listeners
		viewHistoryButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(user.getMessageHistory() == null || user.getMessageHistory().length == 0) {
					if(user.getMessageHistory() == null || user.getMessageHistory().length == 0) {
						JOptionPane.showMessageDialog(frame, "There is no message history for the selected contact");
					} else {
						controller.update(null, Event.HISTORY);
					}
				} else {
					controller.update(null, Event.HISTORY);
				}
			}
		});
		addContactButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.update(null, Event.ADD_CONTACTS);
			}
		});
		removeContactButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// If no contact was selected...
				if (user.getSelectedContact() == null) {
					JOptionPane
					.showMessageDialog(frame, "No contact selected!");
				} else {

					int n = JOptionPane
							.showConfirmDialog(
									frame,
									"Are you sure you want to remove the selected contact?",
									"Confirmation", JOptionPane.YES_NO_OPTION);

					if (n == JOptionPane.YES_OPTION) {
						user.removeContact();
					}
				}
			}
		});
		sendMessageButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				//need to create chat session and open chat window
				boolean success = user.createChatSession(null, null);
				if(!success) {
					JOptionPane.showMessageDialog(frame, "Cannot send message to the selected contact");
				}
			}
		});

		//create panel for buttons
		buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(2,2));
		//add buttons to panel
		buttonPanel.add(sendMessageButton);
		buttonPanel.add(viewHistoryButton);
		buttonPanel.add(addContactButton);
		buttonPanel.add(removeContactButton);

		//create title label for frame
		JLabel title = new JLabel("Contacts");
		title.setHorizontalTextPosition(JLabel.CENTER);
		title.setAlignmentX(Component.CENTER_ALIGNMENT);

		//create scroll pane to hold list of contact names
		JScrollPane scrollPane = new JScrollPane(list);

		//set frame properties and add components
		frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
		frame.add(title);
		frame.add(scrollPane, BorderLayout.CENTER);
		frame.add(buttonPanel,BorderLayout.PAGE_END);
		frame.pack();
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				user.logoff();
				System.exit(0);
			}
		});
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.setVisible(true);
	}

	private JPanel buttonPanel;
	private JButton viewHistoryButton;
	private JButton addContactButton;
	private JButton removeContactButton;
	private JButton sendMessageButton;
	private JList<String> list;
	private User user;
	private GUIController controller;
}
