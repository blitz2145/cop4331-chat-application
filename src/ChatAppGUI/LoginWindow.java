package ChatAppGUI;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import ChatApp.Event;
import ChatApp.User;

/**
 * This class displays the login window
 */
public class LoginWindow extends CWindow {
	/**
	 * Constructs a new LoginWindow
	 * @param user
	 * @param controller
	 */
	public LoginWindow(final User user, final GUIController controller) {
		this.user = user;
		frame = new JFrame();
		frame.setLayout(new GridLayout(3,2));
		
		//create text fields, labels, and buttons
		usernameField = new JTextField(25);
		passwordField = new JPasswordField(25);
		loginButton = new JButton("Login");
		createAccountButton = new JButton("Create New Account");
		
		//add action listeners to buttons
		loginButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				user.login(usernameField.getText(), passwordField.getPassword());
			}
		});
		
		//action listener needs to be implemented
		createAccountButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.update(null, Event.CREATE_ACCOUNT);
			}
		});
		
		//add components to frame
		frame.add(new JLabel("Username"));
		frame.add(usernameField);
		frame.add(new JLabel("Password"));
		frame.add(passwordField);
		frame.add(loginButton);
		frame.add(createAccountButton);
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	private JTextField usernameField;
	private JPasswordField passwordField;
	private JButton loginButton;
	private JButton createAccountButton;
	//suppress "user is unused" warning - it is used in an action listener
	@SuppressWarnings("unused")
	private User user;
	//suppress "user is unused" warning - it is used in an action listener
	@SuppressWarnings("unused")
	private GUIController controller;
}
