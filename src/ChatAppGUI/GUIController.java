package ChatAppGUI;
import java.util.*;

import javax.swing.JOptionPane;

import ChatApp.Event;
import ChatApp.User;

/**
 * This class manages the windows for the application.
 * It will act as a concrete observer of the User class, 
 * where User is the subject, and java.util.Observer is the observer 
 */
public class GUIController implements Observer {
	
	/**
	 * Constructs a new GUIController
	 * @param user the model for the MVC architecture
	 * @precondition user != null
	 */
	public GUIController(User user) {
		assert user != null : "violated precondition user != null";
		this.user = user;
		currentView = new LoginWindow(user, this);
		viewStack = new Stack<CWindow>();
	}

	/**
	 * This method will update the view upon receiving an event from the User object.
	 * @param arg0 the observable object
	 * @param arg1 the Event that is passed to the observer
	 */
	@Override
	public void update(Observable arg0, Object arg1) {
		switch((Event)arg1) {
		//This event is sent when the user logs in
		case LOGIN:
			viewStack.push(currentView);
			currentView.dispose();
			currentView = new ContactsWindow(user, this);
			break;
		case LOGIN_FAIL:
			JOptionPane.showMessageDialog(currentView.frame, "Login Failed");
			break;
		//sent when the user clicks the "Add Contact" button
		case ADD_CONTACTS:
			viewStack.push(currentView);
			currentView = new AddContactWindow(user, this);
			break;
		//sent when the user initiates a chat session
		case CHAT:
			if (!(currentView instanceof ChatWindow)){
				viewStack.push(currentView);
				currentView = new ChatWindow(user, this);
			}
			break;
		//sent when the user clicks the "View Message History" button
		case HISTORY:
			viewStack.push(currentView);
			currentView = new HistoryView(user, this);
			break;
		//sent when the user clicks the "Create New Account" button
		case CREATE_ACCOUNT:
			viewStack.push(currentView);
			currentView = new CreateAccountWindow(user, this);
			break;
		//sent when a contact has been added or removed
		case CONTACT_LIST_CHANGED:
			currentView.dispose();
			currentView.repaint();
			break;
		//sent when a message has been sent
		case SEND_MESSAGE:
			currentView.dispose();
			currentView.repaint();
			break;
		//sent when a message has been recieved
		case REC_MESSAGE:
			currentView.dispose();
			currentView.repaint();
			break;
		//sent when a window has been closed
		case WINDOW_CLOSED:
			currentView = viewStack.pop();
		default:
			break;
		}
	}
	private User user;
	private CWindow currentView;
	private Stack<CWindow> viewStack;
}
