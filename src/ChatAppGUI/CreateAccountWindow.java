package ChatAppGUI;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import ChatApp.User;

/**
 * This class displays a window for the user to create a new account 
 */
public class CreateAccountWindow extends CWindow {
	
	/**
	 * Constructs a new CreateAccountWindow
	 * @param user
	 * @param controller
	 */
	public CreateAccountWindow(final User user, final GUIController controller) {
		this.user = user;
		this.controller = controller;
		repaint();
	}
	
	@Override
	public void repaint() {
		frame = new JFrame();
		
		//create components
		usernameField = new JTextField(20);
		passwordField = new JPasswordField(20);
		passwordConfirmationField = new JPasswordField(20);
		emailField = new JTextField(20);
		submitButton = new JButton("Submit");
		cancelButton = new JButton("Cancel");
		
		//add action listeners
		submitButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO: need to verify that password and confirmation match
				//before this can be called
				frame.dispose();
				user.createAccount(usernameField.getText(), 
						passwordField.getPassword(), emailField.getText());
			}
		});
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
		
		//set frame properties and add components
		frame.setLayout(new GridLayout(5, 2));
		frame.add(new JLabel("Username"));
		frame.add(usernameField);
		frame.add(new JLabel("Password"));
		frame.add(passwordField);
		frame.add(new JLabel("Password Confirmation"));
		frame.add(passwordConfirmationField);
		frame.add(new JLabel("Email Address"));
		frame.add(emailField);
		frame.add(submitButton);
		frame.add(cancelButton);
		frame.pack();
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosed(WindowEvent e) {
				//controller.update(null, Event.WINDOW_CLOSED);
			}
		});
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setVisible(true);
	}
	
	private JTextField usernameField;
	private JPasswordField passwordField;
	private JPasswordField passwordConfirmationField;
	private JTextField emailField;
	private JButton submitButton;
	private JButton cancelButton;
	@SuppressWarnings("unused")
	private GUIController controller;
	private User user;
}
