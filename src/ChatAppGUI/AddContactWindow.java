package ChatAppGUI;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import ChatApp.Event;
import ChatApp.User;

/**
 * This class displays a window that allows the user to add a contact
 */
public class AddContactWindow extends CWindow {
	
	/**
	 * Constructs a new AddContactWindow
	 * @param user the user that will be adding the contact
	 * @param controller the controller that will handle opening and closing windows
	 */
	public AddContactWindow(final User user, final GUIController controller) {
		this.user = user;
		this.controller = controller;
		frame = new JFrame();
		frame.setLayout(new FlowLayout());
		
		//create UI components
		usernameField = new JTextField(20);
		addButton = new JButton("Add");
		//addButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		//add action listener
		addButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				if (usernameField.getText().isEmpty()) {
					JOptionPane.showMessageDialog(frame, "No Username entered!");
				} else {				
					controller.update(null, Event.WINDOW_CLOSED);
					user.addContact(usernameField.getText());
					frame.dispose();
				}
			}
		});
		cancelButton = new JButton("Cancel");
		
		cancelButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.update(null, Event.WINDOW_CLOSED);
				frame.dispose();
			}
		});
		
		//set frame properties and add components
		frame.add(usernameField);
		frame.add(addButton);
		frame.add(cancelButton);
		frame.setPreferredSize(new Dimension(300, 100));
		frame.pack();
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				controller.update(null, Event.WINDOW_CLOSED);
			}
		});
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setVisible(true);
	}
	
	//suppress "user is not used" warning - it is used in an action listener
	@SuppressWarnings("unused")
	private User user;
	private JButton addButton;
	private JButton cancelButton;
	private JTextField usernameField;
	//suppress "controller is not used" warning - it is used in an action listener
	@SuppressWarnings("unused")
	private GUIController controller;
}
