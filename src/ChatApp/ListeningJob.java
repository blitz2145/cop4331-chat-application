package ChatApp;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

//import ChatAppServer.AccountDB;
//import ChatAppServer.ClientHandler;

/**
 * Opens a ServerSocket on port 4445
 * and listens for remote clients trying
 * to chat with the local user
 */
public class ListeningJob implements Runnable {
	public ListeningJob(ContactList c, User u) {
		// Socket to bind to.
		listenSocket = null;
		
		// Access to ContactList
		contacts = c;
		
		// Access to the User to
		// create a chat session
		user = u;

		// Bind to port
		try {
			listenSocket = new ServerSocket(4445);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		String data = null;
		try {
			System.out.println("Listening thread Active");
			while (true) {
				// Blocks until client connects
				System.out.println("Waiting for Client...");
				Socket rockem = listenSocket.accept();

				// Client connected
				System.out.println("Client "
						+ rockem.getInetAddress().toString()
						+ " connected on PORT: " + rockem.getLocalPort());

				// Read message
				in = new BufferedReader(new InputStreamReader(
						rockem.getInputStream()));

				// Pipe data through a character-to-byte generating chain stream
				// and out through the socket to the server
				out = new PrintWriter(rockem.getOutputStream(), true);

				// Read in the remote client's message sender
				while ((data = in.readLine()) != null) {
					if (!data.isEmpty()) {
						System.out.println("Message from Client: " + data);
						String reply = parseMessage(data.trim());
						if (reply == null) {
							System.out.println("MESSAGE PARSING FAILED");
							break;
						// If they're on our contacts list
						} else if (reply.equals("CONTACT_FOUND")) {
							Contact remote = new Contact(bodyData);
							remote.setIpAddress(rockem.getInetAddress());
							
							// then bring up a new chat window
							if(! user.chatActive()) {
								user.createChatSession(remote, rockem);
							}
							user.recieveMessage(messageData, remote, rockem);
						} else {
							rockem.close();
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Close the server socket and release the port
		try {
			listenSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Parses messages from chat clients trying to access the database
	 * using our textual protocol. See the EchoServer class for a
	 * description of the message protocol.
	 * 
	 * @param message	the message to parse and act on.
	 * @throws Exception	thrown when the parser fails to parse a message
	 * 
	 * TODO: Use a custom exception class to throw a parsing exception
	 * or the like. Consider passing messages in JSON format. Check for empty
	 * message using isEmpty.
	 */
	public String parseMessage(String message) throws Exception {
		String action = null;
		String body;
		bodyData = null;
		
		if (message != null) {
			action = message.substring(0, message.indexOf(" "));
			body = message.substring(message.indexOf(" "), message.length());
			body = body.trim();
			
			// Grab username
			bodyData = body.substring(0, body.indexOf(" "));
			if (bodyData == null) {
				System.out.println("MESSAGE CORRUPTED");
				throw new Exception();
			}
			
			// Grab remote user's message
			messageData = body.substring(body.indexOf(" "), body.length());
			
		}

		if (action.equals("MESSAGE")) {
			int result = -1;
			
			// If sender is in our contact's list
			result = contacts.find(bodyData);
			if (result >= 0) {
				System.out.println("Opening new Chat Session");
				return "CONTACT_FOUND";
			}
		}
		
		return null;
	}

	private ServerSocket listenSocket;
	private BufferedReader in;
	@SuppressWarnings("unused")
	private PrintWriter out;
	private ContactList contacts;
	private User user;
	private String bodyData;
	private String messageData;

}
