package ChatApp;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class Connection {

	/**
	 * Creates a socket connection object to the webservice
	 * main and polling threads share the connection object
	 * messages to the server are sent on the main thread.
	 * 
	 * @param reciever	the remote client to connect to
	 * @param remoteSock	the socket the remote client has connected
	 * 						on.
	 */
	public Connection(Contact reciever, Socket remoteSock) {
		// Try to create a socket, or catch the IOException
		try {
			if (reciever == null && remoteSock == null) {
				// Make a connection to the remote host
				echoSocket = new Socket("ec2-50-17-28-145.compute-1.amazonaws.com", 4444);
				 //echoSocket = new Socket("127.0.0.1", 4444);
			} else if (remoteSock == null) {
				// Make a connection to the remote client
				echoSocket = new Socket(reciever.getIpAddress(), 4445);
			} else {
				// The remote client connected to us.
				echoSocket = remoteSock;
			}
			System.out.println("Connection on main thread made...");
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Done on main thread
		// Prints data to the screen
		out = null;
		// Buffer Input data arriving from our socket
		in = null;
		// Data
		message = null;

		// Pipe data through a character-to-byte generating chain stream
		// and out through the socket to the server
		try {
			out = new PrintWriter(echoSocket.getOutputStream(), true);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Buffers data stream from socket that's coming into our client
		// InputStreamReader generates characters from the bytes given by
		// the socket
		try {
			in = new BufferedReader(new InputStreamReader(
					echoSocket.getInputStream()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Given a list of usernames, this method starts a new thread to poll the
	 * server every thirty seconds for contacts that are online
	 * 
	 * @param names	the list of usernames to contact the server about
	 * 
	 */
	public void startPolling(String[] names) {
		Runnable r = new PollingJob(names);
		Thread pollingThread = new Thread(r);
		pollingThread.start();
		System.out.println("Polling thread started...");
	}

	/**
	 * Launches listening thread for clients Called after login
	 * 
	 * @param contacts the contact list needed for the ListeningJob
	 * @param user	   the User who is logged in to the local client
	 */
	public void startListening(ContactList contacts, User user) {
		Runnable r = new ListeningJob(contacts, user);
		Thread listeningThread = new Thread(r);
		listeningThread.start();
		System.out.println("listening thread started...");
	}

	/**
	 * Send a message on the current connection and wait
	 * for a reply
	 * 
	 * @param text the text to send in the message
	 * @return the server/client response
	 */
	public String sendMessage(String text) {
		// Send our data
		out.println(text);
		System.out.println("Sent:\n" + text);

		// Recieve the data
		try {
			while ((message = in.readLine()) != null) {
				if (!message.isEmpty()) {
					System.out.println("Replied: " + message);
					return message;
				}
			}
		} catch (java.net.SocketException ex) {
			System.out.println("Server got disconnected, cleaning up...");
			// Network connections are minefields of exceptions...
			try {
				echoSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			// Gotta catch'em all...
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Send a message on the current connection
	 *  
	 * @param text the text to send in the message
	 * @return null
	 */
	public String singleSendMessage(String text) {
		// Pipe data through a character-to-byte generating chain stream
		// and out through the socket to the server
		try {
			out = new PrintWriter(echoSocket.getOutputStream(), true);
		} catch (IOException e) {
			e.printStackTrace();
		}
		// Send our data
		out.println(text);
		System.out.println("Sent:\n" + text);
		closeConnection();
		return null;
	}
	
	
	/**
	 * Recieve a message on the current connection
	 * 
	 * @return the message recieved from the client or server
	 */
	public String singleRecMessage() {
		// Recieve the data
		try {
			while ((message = in.readLine()) != null) {
				if (!message.isEmpty()) {
					System.out.println("Replied: " + message);
					return message;
				}
			}
		} catch (java.net.SocketException ex) {
			System.out.println("Server got disconnected, cleaning up...");
			// Network connections are minefields of exceptions...
			try {
				echoSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			// Gotta catch'em all...
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Close the current connection to a client/server
	 */
	public void closeConnection() {
		try {
			echoSocket.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private Socket echoSocket;
	private PrintWriter out;
	private BufferedReader in;
	private String message;
}
