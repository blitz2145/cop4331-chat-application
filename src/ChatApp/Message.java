package ChatApp;
import java.util.Date;

/**
 * This class manages the contents of a message
 */
public class Message {

	/**
	 * Constructs a new Message
	 * @param message the text that will comprise the message
	 */
	// TODO: Add contact arguement
	public Message(String message) {
		text = message;
		timestamp = new Date();
	}

	/**
	 * Accessor for the date of the message
	 * @return a Date containing the time that the message was created
	 */
	public Date getTimestamp() {
		return timestamp;
	}

	/**
	 * Accessor for the message text
	 * @return message text
	 */
	public String getText() {
		return text;
	}
	
	/**
	 * Generates a string representation of the Message
	 * @return A String containing the timestamp and text of the message
	 */
	public String toString() {
		return "[" + timestamp.toString() + "]\t" + getText();
	}
	
	private String text;
	private Date timestamp;
}
