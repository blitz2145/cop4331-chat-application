package ChatApp;

import ChatAppGUI.GUIController;

/**
 * This class will launch the application
 */
public class Chat {
	public static void main(String[] args) {
		User user = new User();
		GUIController controller = new GUIController(user);
		user.addObserver(controller);
	}
}
