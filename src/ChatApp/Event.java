package ChatApp;
/**
 * This enum represents the states of the GUI Controller
 */
public enum Event {
	LOGIN, ADD_CONTACTS, CHAT, HISTORY, CREATE_ACCOUNT, SEND_MESSAGE, CONTACT_LIST_CHANGED, WINDOW_CLOSED, REC_MESSAGE, LOGIN_FAIL
}
