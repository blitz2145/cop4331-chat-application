package ChatApp;
import java.net.Socket;
import java.util.*;

/**
 * This class manages a chat session between the user and a contact,
 * including managing a collection of messages.
 * It uses the Strategy pattern in the sortMessages method.
 * The Context in this case is ArrayList<Message>, the Strategy is
 * the Comparator, and the Concrete Strategy is the anonymous class 
 * that implements the Comparator interface.
 */
public class ChatSession {

	/**
	 * Creates a new chat session
	 * @param receiver the contact that the chat session will be with
	 */
	public ChatSession(Contact receiver, Socket remoteSock) {
		messages = new ArrayList<Message>();
		this.receiver = receiver;
		timeStamp = new Date();
		
		// Create a new Connection object
		// connected to the receiver
		if (remoteSock == null) {
			recConnection = new Connection(receiver, null);
		} else {
			recConnection = new Connection(receiver, remoteSock);
		}
	}

	/**
	 * Sends a message to the receiver and stores the message in a list
	 * @param messageText
	 */
	public void sendMessage(String messageText, String from) {
		String message = "MESSAGE ";
		message += from;
		message += " ";
		message += messageText;
		message += "\n\n";

		String reply = recConnection.singleSendMessage(message);

		if(reply == null) {
			// Message sent
			System.out.println("Sent message...");
		// TODO: remove these branches
		} else if (reply.equals("USER_SUCCESS")) {
			
		} else if (reply.equals("USER_FAIL")) {
			System.out.println("Account creation attempt failed...");
			System.out.println("Server responded: " + reply);
			// TODO: Send event to popup error dialog
			
		} else {
			// Something went wrong
		}
		//add message to list
		messages.add(new Message(messageText));
	}

	/**
	 * Called to add a new message to the current conversation
	 * TODO: Should only ever be called by main
	 * thread as a chat session is happening
	 */
	public synchronized void recieveMessage(String messageText) {
		messages.add(new Message(messageText));
	}

	/**
	 * @return The contact that the chat session was with
	 */
	public Contact getContact() {
		return receiver;
	}
	
	/**
	 * Creates a String[] representation of the messages in this chat session
	 * @return the messages in this chat session
	 */
	public String[] getMessages() {
		String[] messageStrings = new String[messages.size()];
		int i = 0;
		for(Message message: messages) {
			messageStrings[i] = message.toString();
			i++;
		}
		return messageStrings;
	}
	
	/**
	 * Sorts messages in the chat session
	 * @param reverse if true, messages will be sorted in reverse chronological order
	 */
	public void sortMessages(boolean reverse) {
		if(reverse == true) {
			//sort in reverse chronological order
			Collections.sort(messages, new Comparator<Message> () {
				@Override
				public int compare(Message arg0, Message arg1) {
					return arg1.getTimestamp().compareTo(arg0.getTimestamp());
				}
			});
		} else {
			//sort in chronological order
			Collections.sort(messages, new Comparator<Message> () {
				@Override
				public int compare(Message arg0, Message arg1) {
					return arg0.getTimestamp().compareTo(arg1.getTimestamp());
				}
			});
		}
	}
	
	/**
	 * 
	 * @return the date and time that the chat session was created
	 */
	public Date getTimeStamp() {
		return timeStamp;
	}
	
	public void setConnection(Socket remoteS) {
		recConnection = new Connection(receiver, null);
	}
	
	private ArrayList<Message> messages;
	private Contact receiver;
	private Date timeStamp;
	private Connection recConnection;
}
