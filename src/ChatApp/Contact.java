package ChatApp;
import java.io.*;
import java.net.InetAddress;

/**
 * This class contains the username and contact information
 * for a contact
 */
public class Contact implements Serializable {
		/**
		 * Constructs a new Contact
		 * @param username the contact's username
		 * @precondition username != null
		 */
        public Contact(String username) {
        		assert username != null :  "violated precondition:  username != null";
                this.username = username;
                ipAddress = null;
        }
        
        /**
         * Accessor for the username of the Contact
         * @return The contact's username
         */
        public String getUsername() {
                return username;
        }

        /**
         * Accessor for the ip address of the contact
         * @return The contact's ip address
         */
        public InetAddress getIpAddress() {
                return ipAddress;
        }

        /**
         * This method sets the contact's ip address
         * @param ipAddress The new ip address
         */
        public void setIpAddress(InetAddress ipAddress) {
                this.ipAddress = ipAddress;
        }

        private String username;
        private InetAddress ipAddress;
        private static final long serialVersionUID = 1L;
}
