package ChatApp;
import java.io.*;
import java.net.InetAddress;
import java.util.*;

/**
 * This class manages a list of contacts
 */
public class ContactList {
	
	/**
	 * Constructs a new contact list by reading data from "ContactList.dat".
	 * If the file is not found, an empty list will be created
	 */
	//suppress unchecked cast exception when deserializing contact list
	@SuppressWarnings("unchecked")
	public ContactList() {
		try {
			ObjectInputStream in = new ObjectInputStream(new FileInputStream("ContactList.dat"));
			list = (ArrayList<Contact>)in.readObject();
			in.close();
			Contact tmp = new Contact("test2");
			tmp.setIpAddress(InetAddress.getByName("192.168.1.136"));
			//list.add(tmp);
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			list = new ArrayList<Contact>();
			System.out.print("Contact List file not found...");
			//e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Selects a contact from the list
	 * @param index the index of the contact to be selected
	 * If (index < 0 || index > list.size() -1) the contact selected will be null
	 */
	public void setSelected(int index) {
		if(index < 0 || index > list.size() -1) {
			selectedContact = null;
		}
		selectedContact = list.get(index);
	}
	
	/**
	 * Accessor for the selected Contact
	 * @return the selected Contact
	 */
	public Contact getSelected() {
		return selectedContact;
	}
	
	/**
	 * Adds a contact to the list
	 * @param contact the contact to be added
	 * @return true
	 * @precondition contact != null
	 */
	public boolean add(Contact contact) {
		assert contact != null : "violated precondition contact != null";
		return list.add(contact);
	}
	
	/**
	 * Removes a contact from the list
	 * @param contact the contact to be removed
	 * @precondition contact != null
	 */
	public void remove(Contact contact) {
		assert contact != null : "violated precondition contact != null";
		list.remove(contact);
	}
	
	/**
	 * Given a username, this method searches the list of contacts for this
	 * username and returns the index of the contact in the list if found.
	 * It returns -1 if the username is not found.
	 * 
	 * @param
	 * 		username the username to find in the contacts list
	 * @returns (index >= 0) if the username is found in the contact list
	 * 			-1			 if the username is not found in the contact list
	 */
	public int find(String username) {
		int i = 0;
		for(Contact contact : list) {
			if(username.equals(contact.getUsername())){
				return i;
			}
			i++;
		}
		return -1;
	}
	
	/**
	 * Creates a string array of the contacts' usernames
	 * @return the usernames
	 */
	public String[] getNames() {
		if(list == null) {
			return null;
		}
		String[] names = new String[list.size()];
		int i = 0;
		for(Contact contact : list) {
			names[i] = contact.getUsername();
			i++;
		}
		return names;
	}
	
	/**
	 * Saves the contact list to file
	 */
	public void saveContacts() {
		try {
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("ContactList.dat"));
			out.writeObject(list);
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @return the number of contacts in the list
	 */
	public int size() {
		return list.size();
	}
	
	private ArrayList<Contact> list;
	private Contact selectedContact;
}
