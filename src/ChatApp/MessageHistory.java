package ChatApp;
import java.util.ArrayList;
import java.util.*;

/**
 * This class manages the message history for the user.
 * It uses the Strategy pattern in the sortHistory method.
 * The Context in this case is ArrayList<ChatSession>, the Strategy is
 * the Comparator, and the Concrete Strategy is the anonymous class 
 * that implements the Comparator interface.
 */
public class MessageHistory {

	/**
	 * Constructs a new MessageHistory object
	 */
	public MessageHistory() {
		history = new ArrayList<ChatSession>();
	}

	/**
	 * Adds a chat session to the message history
	 * @param session
	 */
	public void addHistory(ChatSession session) {
		history.add(session);
	}

	/**
	 * Finds the message history with a particular Contact
	 * @param contact The contact to get the history of
	 * @return The message history with a particular contact
	 */
	public String[] getHistory(Contact contact) {
		//arraylist will contain all messages from all sessions with contact
		ArrayList<String> messages = new ArrayList<String>();
		for(ChatSession session : history) {
			if(session.getContact().equals(contact)) {
				for(String message : session.getMessages()) {
					messages.add(message);
				}
			}
		}
		int i = 0;
		String[] messageStrings = new String[messages.size()];
		for(String string : messages) {
			messageStrings[i] = string;
			i++;
		}
		return messageStrings;
	}
	
	/**
	 * Changes the sort order for all chat sessions in history
	 * @param reverse if true, messages will be sorted in reverse chronological order
	 */
	public void sortHistory(boolean reverse) {
		for(ChatSession session : history) {
			session.sortMessages(reverse);
		}
		if(reverse == true) {
			Collections.sort(history, new Comparator<ChatSession> () {
				@Override
				public int compare(ChatSession arg0, ChatSession arg1) {
					return arg1.getTimeStamp().compareTo(arg0.getTimeStamp());
				}
			});
		} else {
			Collections.sort(history, new Comparator<ChatSession> () {
				@Override
				public int compare(ChatSession arg0, ChatSession arg1) {
					return arg0.getTimeStamp().compareTo(arg1.getTimeStamp());
				}
			});
		}
	}
	private ArrayList<ChatSession> history;
}
