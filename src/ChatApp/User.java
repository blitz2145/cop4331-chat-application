package ChatApp;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.*;

//import javax.swing.ListModel;

/**
 * This class contains and manages the user's data.
 * This class will function as the Model in the Model View Controller Architecture.
 * It also serves as the Subject in the observer pattern.
 */
public class User extends Observable {
	
	/**
	 * Constructs a new User object
	 */
	public User() {
		loggedIn = false;
		// Create a new Connection object
		webServiceConnection = new Connection(null, null);
		history = new MessageHistory();
		// Check for contacts
		// If they have contacts start polling
		
	}
	
	/**
	 * Logs the user in
	 * @param username the username that will be used to log in
	 * @param password the user's password
	 */
	public void login(String username, char[] password) {
		//Validate credentials
		String message = "LOGIN ";
			   message += username;
			   message += " ";
			   message += new String(password);
			   message += "\n\n";
		
		String reply = webServiceConnection.sendMessage(message);

		if (reply.equals("LOGIN_SUCCESS")) {
			loggedIn = true;
			this.username = username;
			
			// Fetch the user's local contacts
			getUserInfo();
			
			setChanged();
			notifyObservers(Event.LOGIN);
			clearChanged();
			
			// Start polling for online contacts
			webServiceConnection.startPolling(getContactNames());
			
			// Start listening for remote clients
			webServiceConnection.startListening(contacts, this);
			
		} else if(reply.equals("LOGIN_FAIL")) {
			System.out.println("Login attempt failed...");
			System.out.println("Server responded: " + reply);
			setChanged();
			notifyObservers(Event.LOGIN_FAIL);
			clearChanged();
		} else {
			// Something went wrong
		}
	}
	
	/**
	 * Logs off the user
	 */
	public void logoff() {
		// Create message
		String message = "LOGOFF ";
			   message += this.username;
			   message += "\n\n";
			   
		// Send the logoff command to the web service
		webServiceConnection.sendMessage(message);
		contacts.saveContacts();
	}
	
	/**
	 * Checks whether the user is logged in
	 * @return true if the user is logged in, false if not
	 */
	public boolean isLoggedIn() {
		return loggedIn;
	}
	
	/**
	 * Selects one of the user's contacts
	 * @param index the index of the contact to be selected
	 * @precondition index >= 0 && index < (numContacts() -1)
	 */
	public void selectContact(int index) {
		assert index >= 0 && index < (numContacts()) :  
			"Violated precondition index >= 0 && index < (numContacts())";
		contacts.setSelected(index);
	}
	
	/**
	 * Gets a String[] of the user's contacts
	 * @return the names of the user's contacts
	 */
	public String[] getContactNames() {
		return contacts.getNames();
	}
	
	/**
	 * Adds a contact to the user's contact list
	 * @param username the username of the contact to be added
	 */
	public void addContact(String username) {
		contacts.add(new Contact(username));
		contacts.saveContacts();
		System.out.println("Contact: " + username + " added successfully");
		setChanged();
		notifyObservers(Event.CONTACT_LIST_CHANGED);
		clearChanged();
	}
	
	/**
	 * Removes the selected contact from the user's contact list
	 * @precondition getSelectedContact() != null
	 */
	public void removeContact() {
		assert getSelectedContact() != null : 
			"Violated precondition getSelectedContact() != null";
		contacts.remove(contacts.getSelected());
		contacts.saveContacts();
		setChanged();
		notifyObservers(Event.CONTACT_LIST_CHANGED);
		clearChanged();
	}
	
	/**
	 * Accesses the selected contact
	 * @return the selected contact
	 */
	public Contact getSelectedContact() {
		return contacts.getSelected();
	}
	
	/**
	 * @return The message history for the selected contact
	 */
	public String[] getMessageHistory() {
		// Need a null check here or else you'll get an exception when you try to view the
		// message history of a contact you've sent no messages too.
		if (history == null) {
			return new String[0];
		}
		return history.getHistory(contacts.getSelected());
	}
	
	/**
	 * Get user information from the web service
	 * 
	 * TODO: Make ContactList constructor throw exceptions so
	 * that this method can catch them and act accordingly.
	 * 
	 * @return true if successful
	 */
	private boolean getUserInfo() {
		/*
		//populate contacts for testing of contacts window
		contacts = new ContactList(new ArrayList<Contact>());
		//create contacts
		Contact abcd = new Contact("abcd");
		Contact efgh = new Contact("efgh");
		Contact ijkl = new Contact("ijkl");
		Contact mnop = new Contact("mnop");
		
		//create chat sessions in order to add to history
		ChatSession session1 = new ChatSession(abcd);
		ChatSession session2 = new ChatSession(efgh);
		ChatSession session3 = new ChatSession(ijkl);
		ChatSession session4 = new ChatSession(mnop);
		
		//add messages to sessions
		for(int i = 0; i < 5; i++) {
			session1.sendMessage("This is message" + i);
			session2.sendMessage("This is message" + i);
			session3.sendMessage("This is message" + i);
			session4.sendMessage("This is message" + i);
		}
		
		//add contacts to contact list
		contacts.add(abcd);
		contacts.add(efgh);
		contacts.add(ijkl);
		contacts.add(mnop);
		
		//create message history
		history = new MessageHistory();
		//add chat sessions to history
		history.addHistory(session1);
		history.addHistory(session2);
		history.addHistory(session3);
		history.addHistory(session4);
		*/
		contacts = new ContactList();
		return true;
	}
	
	/**
	 * Creates a new account for the user on the server
	 * @param username the desired username
	 * @param password the user's password
	 * @param email the user's email address
	 */
	public void createAccount(String username, char[] password, String email) {
		// TODO: Check for username availability
		
		// Construct protocol message to create an account
		// with the desired username, password, email
		String message = "CREATE_USER ";
		message += username;
		message += " ";
		message += new String(password);
		message += " ";
		message += email;
		message += "\n\n";

		String reply = webServiceConnection.sendMessage(message);

		if (reply.equals("USER_SUCCESS")) {
			setChanged();
			notifyObservers(Event.WINDOW_CLOSED);
			clearChanged();
			// The account was created lets login our user
			login(username, password);
		} else if (reply.equals("USER_FAIL")) {
			System.out.println("Account creation attempt failed...");
			System.out.println("Server responded: " + reply);
			// TODO: Send event to popup error dialog
			
		} else {
			// Something went wrong
		}
	}
	
	/**
	 * Creates a chat session with the given reciever. The chat session may be initiated
	 * by the user or by the remote client accordingly.
	 * 
	 * TODO: Avoid fetching the IP of the reciever if the polling thread
	 * has already pulled down their IP. 
	 * 
	 * @return true if successful
	 */
	public boolean createChatSession(Contact reciever, Socket remoteSock) {
		Contact rec = null;
		
		// The reciever and remoteSock is null when the user initiates the chat session
		if (reciever == null && remoteSock == null) {
			rec = contacts.getSelected();
		// The reciever is not empty when a remote contact initiates the chat session
		} else {
			rec = reciever;
		}
		
		// If we have a contact object with no IP set
		if(rec != null && rec.getIpAddress() == null) {
			
			// Get the reciever's ip address
			// from the server
			String message = "GET_IP ";
			message += rec.getUsername();
			message += "\n\n";

			String reply = webServiceConnection.sendMessage(message);
			
			if (reply == null) {
				System.out.println("Getting contact IP failed...");
				return false;
			} else if (reply.equals("FAIL")) {
				System.out.println("Getting contact IP failed...");
				System.out.println("Server responded: " + reply);
				return false;
			} else {
				try {
					// Set the IP address of the reciever
					rec.setIpAddress(InetAddress.getByName(reply));
				} catch (UnknownHostException e) {
					System.out.println("Failed to parse contact IP...");
					e.printStackTrace();
				}
			}
			
			System.out.println("Current Session call: " + rec);
			currentSession = new ChatSession(rec, null);
			System.out.println("History call: " + history);
			history.addHistory(currentSession);
			setChanged();
			notifyObservers(Event.CHAT);
			clearChanged();
			
			return true;
		// If contact object already has IP...
		} else if (rec != null && rec.getIpAddress() != null) {
			currentSession = new ChatSession(rec, remoteSock);
			setChanged();
			notifyObservers(Event.CHAT);
			clearChanged();
			
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Saves the current chat session to message history and ends the current chat session
	 */
	public void endChatSession() {
		history.addHistory(currentSession);
		webServiceConnection.closeConnection();
		webServiceConnection = new Connection(null, null);
		currentSession = null;
		System.out.println("Chat session has ended");
	}
	
	/**
	 * Accesses the messages from the current chat session
	 * @return the current messages
	 * @precondition chatActive() == true
	 */
	public String[] getCurrentMessages() {
		assert chatActive() == true : "violated precondition chatActive == true";
		return currentSession.getMessages();
	}
	
	/**
	 * Checks if there is an active chat session
	 * @return true if there is an active chat session
	 */
	public boolean chatActive() {
		if(currentSession != null) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Sends a message
	 * @param text the message to be sent
	 * @precondition chatActive() == true
	 */
	public void sendMessage(String text) {
		assert chatActive() == true : "violated precondition chatActive == true";	
		currentSession.sendMessage(text, username);
		setChanged();
		notifyObservers(Event.SEND_MESSAGE);
		clearChanged();
	}
	
	/**
	 * Recieves a message
	 * @param text the message recieved
	 * @precondition chatActive() == true
	 */
	public void recieveMessage(String text, Contact sender, Socket rem) {
		assert chatActive() == true : "violated precondition chatActive == true";	
		currentSession.recieveMessage(text);
		currentSession.setConnection(rem);
		setChanged();
		notifyObservers(Event.REC_MESSAGE);
		clearChanged();
	}
	
	/**
	 * Changes the sort order of messages in the current session
	 * @param reverse if true, messages will be sorted in reverse chronological order
	 */
	public void sortMessages(boolean reverse) {
		currentSession.sortMessages(reverse);
		setChanged();
		notifyObservers(Event.SEND_MESSAGE);
		clearChanged();
	}
	
	/**
	 * Changes the sort order of messages in the history
	 * @param reverse if true, messages will be sorted in reverse chronological order
	 */
	public void sortHistory(boolean reverse) {
		history.sortHistory(reverse);
		setChanged();
		notifyObservers(Event.SEND_MESSAGE);
		clearChanged();
	}
	
	/**
	 * 
	 * @return the number of contacts that the user has
	 */
	public int numContacts() {
		return contacts.size();
	}
	
	private ChatSession currentSession;
	private boolean loggedIn;
	private ContactList contacts;
	private MessageHistory history;
	private String username;
	private Connection webServiceConnection;
}
