package ChatAppTests;

import static org.junit.Assert.*;

import org.junit.Test;

import ChatApp.Contact;
import ChatApp.ContactList;

public class ContactListTest {

	/**
	 * Tests setSelected(), getSelected(), add(), and find()
	 */
	@Test
	public void testSetSelected() {
		ContactList list = new ContactList();
		Contact c1 = new Contact("abc");
		Contact c2 = new Contact("def");
		Contact c3 = new Contact("ghi");
		list.add(c1);
		list.add(c2);
		list.add(c3);
		int index = list.find("def");
		list.setSelected(index);
		Contact selected = list.getSelected();
		assertTrue(selected.getUsername().equals("def"));
	}

	/**
	 * Tests remove() and size()
	 */
	@Test
	public void testRemove() {
		ContactList list = new ContactList();
		Contact c1 = new Contact("abc");
		Contact c2 = new Contact("def");
		Contact c3 = new Contact("ghi");
		list.add(c1);
		list.add(c2);
		list.add(c3);
		int size = list.size();
		list.remove(c2);
		assertTrue(list.size() == size - 1);
	}

	@Test
	public void testSaveContacts() {
		ContactList list = new ContactList();
		Contact c1 = new Contact("abc");
		Contact c2 = new Contact("def");
		Contact c3 = new Contact("ghi");
		list.add(c1);
		list.add(c2);
		list.add(c3);
		list.saveContacts();
		ContactList list2 = new ContactList();
		assertTrue(list2.size() == (list.size()));
	}

}
