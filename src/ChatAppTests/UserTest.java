package ChatAppTests;

import static org.junit.Assert.*;

import java.net.PasswordAuthentication;

import org.junit.Before;
import org.junit.Test;

import ChatApp.Contact;
import ChatApp.User;

@SuppressWarnings("unused")
public class UserTest {

	@Before
	public void setUp() throws Exception {
		userInstance = new User();
	}
	
	/**
	 * Test method isLoggedIn()
	 */
	@Test
	public void testIsLoggedIn() {
		assertTrue(userInstance.isLoggedIn() == false);
	}
	
	/**
	 * Test method selectContact()
	 */
	@Test(expected=AssertionError.class)
	public void testSelectContact() {
			userInstance.selectContact(-2);
	}

	/**
	 * Test method getCurrentMessages()
	 */
	@Test(expected=AssertionError.class)
	public void testGetCurrentMessages() {
		userInstance.getCurrentMessages();
	}

	/**
	 * Test method chatActive()
	 */
	@Test
	public void testChatActive() {
		assertTrue(userInstance.chatActive() == false);
	}
	
	private User userInstance;
}
