package ChatAppTests;

import static org.junit.Assert.*;

import org.junit.Test;

import ChatApp.Message;

public class MessageTest {

	@Test
	public void testGetText() {
		Message m1 = new Message("abcd");
		assertTrue(m1.getText().equals("abcd"));
	}

}
