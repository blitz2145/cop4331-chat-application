package ChatAppTests;

import static org.junit.Assert.assertTrue;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.junit.Test;

import ChatApp.*;



public class ContactTest {

	/**
	 * Tests constructor and getUsername()
	 */
	@Test
	public void testGetUsername() {
		Contact contact = new Contact("abcd");
		assertTrue(contact.getUsername().equals("abcd"));
	}

	/**
	 * Tests setIpAddress() and getIpAddress()
	 * @throws UnknownHostException
	 */
	@Test
	public void testSetIpAddress() throws UnknownHostException {
		Contact contact = new Contact("abcd");
		contact.setIpAddress(InetAddress.getByName("127.0.0.1"));
		assertTrue(contact.getIpAddress().equals(InetAddress.getByName("127.0.0.1")));
	}

}
