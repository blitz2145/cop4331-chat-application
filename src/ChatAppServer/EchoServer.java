package ChatAppServer;
import java.io.*;
import java.net.*;


public class EchoServer {

	/**
	 * Manages accounts for chat clients that connect to it. Communication is handled
	 * through the following textual protocol:
	 * 
	 * FORMAT:
	 * 	<ACTION>
	 * 		Send:
	 * 			<VERB> [<DATA> | <DATA>... ] <TERMINATOR>
	 * 		Recieve:
	 * 			<VERB> [<DATA> | <DATA>... ] <TERMINATOR>
	 * 
	 * PROTOCOL SPECIFICATION:
	 * 	<LOGIN>
	 * 		Send:
	 * 			LOGIN <USERNAME> <PASSWORD>\n\n
	 * 		Recieve:
	 * 			LOGIN_SUCCESS\n\n
	 * 			or
	 * 			LOGIN_FAIL\n\n
	 * 
	 *  <CHECK USERNAME>
	 * 		Send:
	 * 			CHECK_USER <USERNAME>\n\n
	 * 		Recieve:
	 * 			USER_OK\n\n
	 * 			or
	 * 			USER_TAKEN\n\n
	 * 
	 *  <CREATE ACCOUNT>
	 * 		Send:
	 * 			CREATE_USER <USERNAME> <PASSWORD> <EMAIL>\n\n
	 * 		Recieve:
	 * 			USER_SUCCESS\n\n
	 * 			or
	 * 			USER_FAIL\n\n
	 * 
	 *  <CHECK ONLINE>
	 * 		Send:
	 * 			IS_ONLINE <USERNAME>\n\n
	 * 		Recieve:
	 * 			TRUE\n\n
	 * 			or
	 * 			FALSE\n\n
	 * 
	 *  <SET OFFLINE>
	 * 		Send:
	 * 			LOGOFF <USERNAME>\n\n
	 * 		Recieve:
	 * 			No response
	 * 
	 *  <GET IP>
	 * 		Send:
	 * 			GET_IP <USERNAME>\n\n
	 * 		Recieve:
	 * 			<IP>\n\n
	 * 			or
	 * 			FAIL\n\n
	 * 
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		// Create our account database
		db = new AccountDB();
		
		// Declare a Socket to bind to.
		ServerSocket sockem = null;
		
		// Bind to port 4444
		sockem = new ServerSocket(4444);
		
		try {
			while (true) {
				// Blocks until client connects
				System.out.println("Waiting for Client...");
				Socket rockem = sockem.accept();
				
				// Client connected
				System.out.println("Client " + rockem.getInetAddress().toString() + " connected on PORT: " + rockem.getLocalPort());
				System.out.println("Starting new thread to handle client...");
				
				Runnable r = new ClientHandler(rockem, db);
				Thread clientHandlerThread = new Thread(r);
				clientHandlerThread.start();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// Close the server socket and release the port
		sockem.close();
	}
	
	private static AccountDB db;
}
