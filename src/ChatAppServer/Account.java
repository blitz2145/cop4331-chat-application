package ChatAppServer;

import java.net.InetAddress;

public class Account {
	/**
	 * Constructs an account represent a user on the server
	 */
	public Account(String user, String pass, InetAddress ip, String em) {
		// TODO: Validate info checking for invalid usernames, etc.
		username = user;
		password = pass;
		ipAddress = ip;
		email = em;
		isLoggedIn = false;
	}
	
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the ipAddress
	 */
	public InetAddress getIpAddress() {
		return ipAddress;
	}
	/**
	 * @param ipAddress the ipAddress to set
	 */
	public void setIpAddress(InetAddress ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	/**
	 * @return the login status
	 */
	public boolean getIsLoggedIn() {
		return isLoggedIn;
	}

	/**
	 * @param status the login status to set
	 */
	public void setIsLoggedIn(boolean status) {
		isLoggedIn = status;
	}
	
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	private boolean isLoggedIn;
	private String username;
	private String password;
	private String email;
	private InetAddress ipAddress;
}
