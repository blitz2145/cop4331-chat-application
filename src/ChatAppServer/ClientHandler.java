package ChatAppServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

/**
 * A threaded handler for Chat clients connecting to the server. Parses protocol
 * messages from the chat clients and relays back the correct responses.
 */
public class ClientHandler implements Runnable {

	/**
	 * Initializes the socket object the client has connected with
	 * and sets the AccountDB object to query.
	 */
	public ClientHandler(Socket clientSocket, AccountDB db) {
		try {
			sock = clientSocket;

			InputStreamReader isReader = new InputStreamReader(
					sock.getInputStream());
			reader = new BufferedReader(isReader);

			out = new PrintWriter(clientSocket.getOutputStream(), true);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		this.db = db;
	}

	/**
	 * Reads client data on the socket and sends it to be parsed.
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		String data = null;

		try {
			// Read the message from the client
			while ((data = reader.readLine()) != null) {
				if (!data.isEmpty()) {
					System.out.println("Message from Client: " + data);
					String reply = parseMessage(data.trim());
					if (reply == null) {
						System.out.println("MESSAGE PARSING FAILED");
						out.println("FAIL");
						System.out.println("MESSAGE SENT");
					} else if (reply.equals("LOGOFF\n\n")) {
						break;
					} else {
						out.println(reply);
					}
				}
			}

			out.close();
			sock.close();
		} catch (java.net.SocketException ex) {
			System.out.println("Client got disconnected, cleaning up...");
			// Network connections are minefields of exceptions...
			try {
				sock.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			// Gotta catch'em all...
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Parses messages from chat clients trying to access the database using our
	 * textual protocol. See the EchoServer class for a description of the
	 * message protocol.
	 * 
	 * @param message the message to parse and act on.
	 * 
	 * @throws Exception thrown when the parser fails to parse a message
	 * 
	 * TODO: Use a custom exception class to throw a parsing exception or the
	 * like. Consider passing messages in JSON format. Check for empty message
	 * using isEmpty. Consider adding timeout for clients that die before
	 * sending LOGOFF message, heartbeat of some sort.
	 */
	public String parseMessage(String message) throws Exception {
		String action = null;
		String body;
		String[] bodyData = null;

		if (message != null) {
			action = message.substring(0, message.indexOf(" "));
			body = message.substring(message.indexOf(" "), message.length());
			body = body.trim();
			bodyData = body.split(" ");
			if (bodyData == null) {
				System.out.println("MESSAGE CORRUPTED");
				throw new Exception();
			}
		}

		if (action.equals("LOGIN")) {
			System.out.println("GOT LOGIN REQ, REPLYING");
			// If the username and password are correct...
//			System.out.println("MESSAGE: ");
//			System.out.println("0: " + bodyData[0]);
//			System.out.println("1: " + bodyData[1]);
			if (db.checkCredentials(bodyData[0], bodyData[1])) {
				// Update the account's ip
				db.setIP(bodyData[0], sock.getInetAddress());

				return "LOGIN_SUCCESS\n\n";
			} else {
				return "LOGIN_FAIL\n\n";
			}
		} else if (action.equals("LOGOFF")) {
			// Send message back to exit our message loop
			return "LOGOFF\n\n";
		} else if (action.equals("CHECK_USER")) {
			if (db.userIsAvailable(bodyData[0])) {
				return "USER_SUCCESS\n\n";
			} else {
				return "USER_FAIL\n\n";
			}
		} else if (action.equals("CREATE_USER")) {
			if (db.userIsAvailable(bodyData[0])) {
				// Create the account
				db.createAccount(bodyData[0], bodyData[1],
						sock.getInetAddress(), bodyData[2]);
				db.printAccount(bodyData[0]);
				return "USER_SUCCESS\n\n";
			} else {
				return "USER_FAIL\n\n";
			}
		} else if (action.equals("IS_ONLINE")) {

		} else if (action.equals("GET_IP")) {
			InetAddress result = db.getIP(bodyData[0]);
			if (result == null) {
				return null;
			} else {
				return (result.toString()).substring(1, (result.toString().length()));
			}
		} else {

		}

		return null;
	}

	private AccountDB db;
	private BufferedReader reader;
	private PrintWriter out;
	private Socket sock;
}