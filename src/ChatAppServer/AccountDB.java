package ChatAppServer;

import java.io.*;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

/**
 * In-memory database of the accounts that have been created.
 * Accounts are persisted to a file on server shutdown.
 */
public class AccountDB {
	public AccountDB() {
		accounts = new ArrayList<Account>();
		
		// Create a few dummy accounts
		Account tester = null;
		try {
			tester = new Account("test", "test", InetAddress.getByName("127.0.0.1"), "test@gmail.com");
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		accounts.add(tester);
		
		//TODO: Load Accounts from disk
//		try {
//			ObjectInputStream in = new ObjectInputStream(new FileInputStream("AccountDB.dat"));
//			accounts = (ArrayList<Account>)in.readObject();
//			in.close();
//			
//		} catch (FileNotFoundException e) {
//			accounts = new ArrayList<Account>();
//			System.out.print("AccountDB file not found...");
//			//e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		} catch (ClassNotFoundException e) {
//			e.printStackTrace();
//		}
	}
	
	/**
	 * Looks through the accounts database to find an account that matches the given information
	 * If it finds one it returns true, if not it returns false.
	 * 
	 * @param username	the username of the account to find
	 * @param password	the password of the account to find
	 * @return 	true	if the account is found
	 * 			false	if the account is NOT found
	 */
	public boolean checkCredentials(String username, String password) {
		for (Account acct : accounts) {
			if (acct.getUsername().equals(username) && acct.getPassword().equals(password)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Looks through the accounts database to find an account that matches the given information
	 * And gets the account's ip.
	 * 
	 * @param username	the username of the account to find
	 */
	public InetAddress getIP(String username) {
		for (Account acct : accounts) {
			if (acct.getUsername().equals(username)) {
				return acct.getIpAddress();
			}
		}
		return null;
	}
	
	/**
	 * Looks through the accounts database to find an account that matches the given information
	 * And sets the account's ip.
	 * 
	 * @param username	the username of the account to find
	 * @param ipAddress	the ipAddress of the account
	 */
	public void setIP(String username, InetAddress ipAddress) {
		for (Account acct : accounts) {
			if (acct.getUsername().equals(username)) {
				acct.setIpAddress(ipAddress);
			}
		}
	}
	
	/**
	 * Writes all accounts in the database to a file
	 * using their serial representation
	 */
	public void persist() {
		try {
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("AccountDB.dat"));
			out.writeObject(accounts);
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Looks through the accounts database to find an account that matches the given information
	 * And sets the account's ip.
	 * 
	 * @param username	the username of the account to find
	 * @return true	if the username isn't in the database
	 * 		   false if the username is in the database
	 */
	public boolean userIsAvailable(String username) {
		for (Account acct : accounts) {
			if (acct.getUsername().equals(username)) {
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Creates an account and adds it to the database.
	 * This method is atomic to avoid race conditions between threads trying to create
	 * accounts and threads trying to read from the database.
	 * 
	 * @param username	the username of the account to be created
	 * @param password	the password of the account to be created
	 * @param ip		the ip address of the connecting client
	 * @param email		the email address of the account to be created
	 */
	public synchronized void createAccount(String username, String password, InetAddress ip, String email) {
		Account newAcct = new Account(username, password, ip, email);
		accounts.add(newAcct);
		// persist();
	}
	
	/**
	 * Looks through the accounts database to find an account that matches the given information
	 * And prints that account's information
	 * 
	 * @param username	the username of the account to find
	 */
	public void printAccount(String username) {
		System.out.println("Searching for: " + username);
		for (Account acct : accounts) {
			if (acct.getUsername().equals(username)) {
				System.out.println(acct.getUsername());
				System.out.println(acct.getPassword());
				System.out.println(acct.getIpAddress());
				System.out.println(acct.getEmail());
			}
		}
	}
	
	private ArrayList<Account> accounts;
}
